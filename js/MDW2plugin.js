/**
 * Created by
 * Luis Alberto Zegarra M.
 * Maribel Rosario Condori Llanos
 * Leticia Gomez Valda
 *  on 06-09-18.
 */
var mdw2Plugin=mdw2Plugin ||{};
var mdw2Plugin2=mdw2Plugin2 ||{};
mdw2Plugin=function(obj){
//    console.log(obj);
    if(obj){
        this.title=obj.title;
        this.message=obj.message;
        this.width=obj.width;
        this.height=obj.height;
        this.types=obj.types;
        this.fields=obj.fields;
        this.closed=obj.closed;
        this.buttons=obj.buttons;
    }
    this.fondo=document.getElementById('zemBody');
    init=function(){
        switch (this.types){
            case 'success':success();
                break;//notice
            case 'warning':warning();
                break;
            case 'notice':notice();
                break;
            case 'close':close();
                break;
            case 'close2':close2();
                break;
        }
    };
    success=function(){
        var contenidoHTML = '<h1 class=\"windowHeader success\"><span class="icon-success"></span>'+this.message+'</h1>';
        var bgdiv=document.createElement('div');
        this.fondo.appendChild(bgdiv);
        bgdiv.setAttribute('class','bgtransparent');
        bgdiv.setAttribute('id','bgtransparent');
        var wscr = document.getElementsByTagName('body')[0].clientWidth;
        var hscr = document.getElementsByTagName('body')[0].clientHeight;
        $('#bgtransparent').css("width", wscr);
        $('#bgtransparent').css("height", hscr);
        var moddiv=document.createElement('div');
        this.fondo.appendChild(moddiv);
        moddiv.setAttribute('class','bgmodal');
        moddiv.setAttribute('id','bgmodal');
        $('#bgmodal').append(contenidoHTML);
        var wscr = $(window).width();
        var hscr = $(window).height();
        $('#bgtransparent').css("width", wscr);
        $('#bgtransparent').css("height", hscr);
        $('#bgmodal').css("width", this.width+'px');
        $('#bgmodal').css("height", this.height+'px');
        var wcnt = $('#bgmodal').width();
        var hcnt = $('#bgmodal').height();

        var mleft = ( wscr - wcnt ) / 2;
        var mtop = ( hscr - hcnt ) / 2;

        $('#bgmodal').css("left", mleft+'px');
        $('#bgmodal').css("top", mtop+'px');

        setTimeout('close()',4000);
    };
    warning=function(){
        var contenidoHTML = '<h1 class=\"windowHeader warning\"><span class="icon-warning" title="Cerrar"  onclick="mdw2Plugin({types:\'close2\'})"></span>'+this.message+'</h1>';

        var moddiv=document.createElement('div');
        this.fondo.appendChild(moddiv);
        moddiv.setAttribute('class','bgMessage');
        moddiv.setAttribute('id','bgMessage');
        $('#bgMessage').append(contenidoHTML);

        var wscr = document.getElementsByTagName('body')[0].clientWidth;
        var hscr = document.getElementsByTagName('body')[0].clientHeight;

        $('#bgMessage').css("width", this.width+'px');
        $('#bgMessage').css("height", this.height+'px');

        var wcnt = $('#bgMessage').width();
        var hcnt = $('#bgMessage').height();

        var mleft = ( wscr - wcnt ) / 2;
        var mtop = ( hscr - hcnt ) / 2;

        $('#bgMessage').css("left", mleft+'px');
        $('#bgMessage').css("top", mtop+'px');
    };
    notice=function(){
        var contenidoHTML = '<h1 class=\"windowHeader notice\"><span class="icon-checkmark"   onclick="mdw2Plugin({types:\'close\'})"></span>'+this.message+'</h1>';
        var bgdiv=document.createElement('div');
        this.fondo.appendChild(bgdiv);
        bgdiv.setAttribute('class','bgtransparent');
        bgdiv.setAttribute('id','bgtransparent');

        var wscr = $(window).width();
        var hscr = $(window).height();

        $('#bgtransparent').css("width", wscr);
        $('#bgtransparent').css("height", hscr);
        // ventana flotante
        var moddiv=document.createElement('div');
        this.fondo.appendChild(moddiv);
        moddiv.setAttribute('class','bgmodal');
        moddiv.setAttribute('id','bgmodal');
        $('#bgmodal').append(contenidoHTML);

        var wscr = $(window).width();
        var hscr = $(window).height();

        // estableciendo dimensiones de background
        $('#bgtransparent').css("width", wscr);
        $('#bgtransparent').css("height", hscr);

        // definiendo tamaño del contenedor
        $('#bgmodal').css("width", this.width+'px');
        $('#bgmodal').css("height", this.height+'px');

        // obtiendo tamaño de contenedor
        var wcnt = $('#bgmodal').width();
        var hcnt = $('#bgmodal').height();

        // obtener posicion central
        var mleft = ( wscr - wcnt ) / 2;
        var mtop = ( hscr - hcnt ) / 2;

        // estableciendo posicion
        $('#bgmodal').css("left", mleft+'px');
        $('#bgmodal').css("top", mtop+'px');

//        setTimeout('close()',2000);
    };
    close=function()
    {
        $("#bgmodal").fadeOut(300,function() { $("#bgmodal").remove(); });
        $("#bgtransparent").fadeOut(300,function() { $("#bgtransparent").remove(); });
    };
    close2=function()
    {
        $("#bgMessage").fadeOut(300,function() { $("#bgMessage").remove(); });
    };

    if(obj){

        init();

    }
}

mdw2Plugin2={
    search:function(word, html)
    {
        html = html.replace(/<span class="finded">(.*?)<\/span>/g, "$1");
        var reg = new RegExp(word.replace(/[\[\]\(\)\{\}\.\-\?\*\+]/, "\\$&"), "gi");
        var htmlreg = /<\/?(?:a|b|br|em|font|img|p|span|strong)[^>]*?\/?>/g;
        var array;
        var htmlarray;
        var len = 0;
        var sum = 0;
        var pad = 28 + word.length;
        while ((array = reg.exec(html)) != null) {
            htmlarray = htmlreg.exec(html);
            if(htmlarray != null && htmlarray.index < array.index && htmlarray.index + htmlarray[0].length > array.index + word.length){
                reg.lastIndex = htmlarray.index + htmlarray[0].length;
                continue;
            }
            len = array.index + word.length;
            html = html.slice(0, array.index) + "<span class='finded'>" + html.slice(array.index, len) + "</span>" + html.slice(len, html.length);
            reg.lastIndex += pad;
            if(htmlarray != null) htmlreg.lastIndex = reg.lastIndex;
            sum++;
        }
        return {total: sum, html: html};
    }
}
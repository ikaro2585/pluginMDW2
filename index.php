<?php
/**
 * Created by PhpStorm.
 * User: ENVY
 * Date: 06-09-18
 * Time: 06:51 PM
 */ ?>
<html>
<head>
    <script src="js/jquery-latest.min.js"></script>
    <script src="js/MDW2plugin.js"></script>
    <link rel="stylesheet" href="css/MDW2plugin.css" type="text/css" />
    <link rel="stylesheet" href="css/MDW2search.css" type="text/css" />
</head>
<body>
<div id="zemBody">
    <div class="controls">
        <input id="search" type="text" />
        <button id="button">Buscar</button>
        <span id="results"></span>
    </div>

    <div id="content">
        <p>Lorem ipsum dolor sit amet consectetur adipiscing elit, porta eleifend curabitur sagittis facilisi primis platea placerat, proin nam vivamus nunc litora convallis. Varius suspendisse at interdum parturient dictum pretium sociis sociosqu, magnis eu pulvinar in class erat tortor sem, dui netus dapibus tincidunt mi viverra arcu. Etiam quisque nascetur sem commodo dis maecenas rhoncus tempor, ante hac tempus iaculis in nullam nibh suspendisse, purus ridiculus vitae sagittis placerat venenatis orci.</p>
        <p><img class="image" align="left" src="img/logo.png" width="200" height="221" />El Lorem Ipsum fue concebido como un texto de relleno, formateado de una cierta manera para permitir la presentación de elementos gráficos en documentos, sin necesidad de una copia formal. El uso de Lorem Ipsum permite a los diseñadores reunir los diseños y la forma del contenido antes de que el contenido se haya creado, dando al diseño y al proceso de producción más libertad....</p>
        <p>Purus nam et curae phasellus conubia lectus massa nisi rhoncus, per convallis pharetra tempus morbi malesuada inceptos erat scelerisque, habitasse pulvinar duis fusce tincidunt enim venenatis vitae. Volutpat sed massa natoque ridiculus pulvinar ultricies a, ac erat blandit dui nostra. Tempus sagittis sociis nisi ornare etiam accumsan eleifend vulputate magna taciti, nec facilisis vehicula in scelerisque dignissim rhoncus non. Facilisi ultrices massa tincidunt augue vitae interdum ullamcorper libero praesent, facilisis enim duis parturient magna diam lectus. Facilisis nullam magna morbi donec odio dis taciti posuere, iaculis ac ut pretium orci blandit nam, leo porta risus rutrum primis aptent congue.</p>
    </div>
</div>
<script>
//    mdw2Plugin({
//        message:'success : Mensaje de Success',
//        types:'success'
//    });

//    mdw2Plugin({
//        message:'Error : Mensaje de Warning',
//        types:'warning'
//    });
    mdw2Plugin({
        message:'Aviso<hr> Esta Pagina le permitira buscar texto',
        types:'notice',
        title:'noticia',
//        width:400,
        height:60
    });
//---Al presionar el botón de buscar
    document.getElementById("button").addEventListener("click", function(){
    var search = document.getElementById("search").value;
    if(search.length == 0) return;
    var props =mdw2Plugin2.search( search, document.getElementById("content").innerHTML );
    document.getElementById("results").innerHTML = (props.total > 0) ? "Veces encontradas: " + props.total : "No se ha encontrado";
    if(props.total > 0) document.getElementById("content").innerHTML = props.html;
});

</script>
</body>
</html>